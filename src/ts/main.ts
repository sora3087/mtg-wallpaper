import App from './classes/App'
import { UpdateCardSettings } from './store/cards/actions'
import { UpdateScryfallSettings } from './store/scryfall/actions'
import { UpdateViewSettings } from './store/view/actions'

declare global {
  interface Window {
    wallpaperPropertyListener: {
      applyUserProperties: (props: any) => void
    },
    process: {
      env: {
        NODE_ENV: string
      }
    }
  }
}

window.wallpaperPropertyListener = window.wallpaperPropertyListener || {
  applyUserProperties: (props) => {
    const updatedOptions: { [key: string]: { [key: string]: any}} = {}
    Object.keys(props).forEach((propKey: string) => {
      const keyParts = propKey.split(/(?=[A-Z])/)
      if(keyParts.length == 0) return;
      
      let key = keyParts.splice(0, 1)[0]
      if(!updatedOptions[key]) updatedOptions[key] = {};
      
      if(keyParts.length == 0) {
        // there was only the base key
        updatedOptions[key] = props[propKey]
      } else {
        keyParts[0] = keyParts[0].toLowerCase()
        let subKey = keyParts.join('')
        updatedOptions[key][subKey] = props[propKey]
      }
    })
    
    if(updatedOptions.card){
      UpdateCardSettings(updatedOptions.card)
    }
    if(updatedOptions.view){
      UpdateViewSettings(updatedOptions.view)
    }
    if(updatedOptions.scryfall){
      UpdateScryfallSettings(updatedOptions.scryfall)
    }
  }
}

document.addEventListener('DOMContentLoaded', init);

function init(){
  new App();
}