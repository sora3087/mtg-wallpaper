import { Object3D } from 'three'
import { State } from './State'
import { Tile } from './Tile'

const CARD_WIDTH = 200
const CARD_HEIGHT = CARD_WIDTH * 7 / 5

export default class Strip extends Object3D{
  private _speed = 1
  children: Tile[] = []
  constructor(speed: number = 1){
    super()
    this._speed = speed
    for (let i = 0; i < State.tileCount; i++) {
      let tile = new Tile(CARD_WIDTH, CARD_HEIGHT)
      tile.speed = this._speed
      tile.position.x = (
        i * CARD_WIDTH
        + (State.tileMargin * (i + 1))
      )
      this.add(tile)
    }
  }
  set speed (val: number){
    if(typeof val == "number" && val !=0)
      this._speed = val
      for (let i = 0; i < this.children.length; i++) {
        this.children[i].speed = this._speed
      }
  }
  update(){
    for (let i = 0; i < this.children.length; i++) {
      this.children[i].update()
    }
  }
  set hovered(val: boolean){

  }
}