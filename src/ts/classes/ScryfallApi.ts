import { store } from '../store'
import { ScryfallState } from '../store/scryfall/types'
import { Card } from '../types/Card.type'
import { FetchThrottle } from './FetchThrottle'

const throttle = new FetchThrottle(10, 1000)

export default class ScryfallApi{
  private base = "https://api.scryfall.com"
  private filter = ""
  constructor(){
    this.filter = store.getState().scryfall.filter
    store.subscribe(() => {
      this.filter = store.getState().scryfall.filter
    })
  }
  get(endpoint: string, params?: { [key: string]: string}): Promise<any>{
    if(params){
      endpoint+="?"+Object.keys(params).map(key => `${key}=${params[key]}`)
    }
    return throttle.fetch(this.base+endpoint)
  }
  cardSearch(q?: string): Promise<Array<Card>>{
    // todo: implement options
    if (!q) q = this.filter
    return this.get('/cards/search', {q})
    .then(resp => resp.json())
    .catch(console.log)
  }
  cardRandom(q?: string): Promise<Card>{
    if (!q) q = this.filter
    return this.get('/cards/random', {q})
    .then(resp => resp.json())
    .catch(console.log)
  }
}