import {
  Group,
  Scene,
  WebGLRenderer,
  OrthographicCamera,
  Raycaster,
  Vector3,
  MathUtils
} from 'three'
import { State } from './State'
import { Background } from './Background'
import Strip from './Strip'
import { mouse } from '../services/mouse.service'
import { store } from '../store'
import { ViewState } from '../store/view/types'

const FAR_PLANE = 2200 //seems to be fine for UHD

export default class App{
  private renderer: WebGLRenderer
  private scene: Scene
  private camera: OrthographicCamera
  private background: Background
  private group: Group
  private strips: Array<Strip> = []
  constructor(){
    this.camera = new OrthographicCamera(
      window.innerWidth / - 2,
      window.innerWidth / 2,
      window.innerHeight / 2,
      window.innerHeight / - 2,
      1,
      FAR_PLANE
    );
    this.renderer = new WebGLRenderer({
      antialias: true
    });
    this.renderer.setPixelRatio(window.devicePixelRatio)
    this.scene = new Scene()

    // need to move cards into strips
    this.group = new Group()
    this.group.rotateX(-45*Math.PI/180)
    this.group.rotateZ(45*Math.PI/180)
    this.group.position.z = FAR_PLANE/-2
    this.scene.add(this.group)

    this.background = new Background()
    this.background.mesh.position.z = FAR_PLANE*-1
    this.scene.add(this.background.mesh)

    document.body.appendChild(this.renderer.domElement)

    window.addEventListener("resize", () => this.resize())
    this.resize()

    this.renderer.setAnimationLoop(this.update)
    
    const state = store.getState().view
    this.handleViewChange(state)
    this.createStrips(state.rows)
    store.subscribe(() => {
      const state = store.getState().view
      this.handleViewChange(state)
      this.updateStrips(state.rows)
    })
  }
  resize(): void;
  resize(w: number, h: number): void;
  resize(p1?: number, p2?: number){
    let w = 0
    let h = 0
    if(typeof p1 == 'number') {
      w = p1
      h = p2 || 0
    } else {
      w = window.innerWidth
      h = window.innerHeight
    }
    this.camera.left =  w / -2
    this.camera.right = w / 2
    this.camera.top = h / 2
    this.camera.bottom = h / -2
    this.camera.updateProjectionMatrix()

    this.renderer.setSize(w, h)
    this.renderer.pixelRatio = window.devicePixelRatio
    this.background.resize()
  }
  private update = (time: number) => {
    this.background.update()
    this.renderer.render(this.scene, this.camera)
    this.strips.forEach(strip => strip.update())
    if(mouse.inView && document.visibilityState == "visible")
      this.handleHover()
  }
  private addStripSet(idx: number){
    let speed = (Math.random() * 2 + 1) * ( (idx%2 == 0) ? -1 : 1)
    const top = new Strip(speed)
    top.position.y = (State.tileSize.h + State.rowMargin) * (idx+1) * -1
    this.group.add(top)
    this.strips.push(top)
    
    speed = (Math.random() * 2 + 1) * ( (idx%2 == 0) ? -1 : 1)
    const bottom = new Strip(speed)
    bottom.position.y = (State.tileSize.h + State.rowMargin) * (idx+1)
    this.group.add(bottom)
    this.strips.push(bottom)
  }
  private createStrips(numStrips: number){
    const strip = new Strip()
    this.strips.push(strip)
    this.group.add(strip)
    for (let i = 0; i < numStrips; i++) {
      this.addStripSet(i)
    }
  }
  private updateStrips(numStrips: number){
    const delta = numStrips- (this.strips.length-1)/2
    if(delta < 0){
      for(let i = 0; i < delta*-2; i++){
        const strip = this.strips.pop()
        if(strip){
          this.group.remove(strip)
        }
      }
    } else if(delta > 0){
      for(let i = 0; i < delta; i++){
        this.addStripSet((this.strips.length-1)/2)
      }
    }
  }
  private handleViewChange(state: ViewState){
    this.group.rotation.set(
      state.rotation.x * MathUtils.DEG2RAD,
      state.rotation.y * MathUtils.DEG2RAD,
      state.rotation.z * MathUtils.DEG2RAD
    )
    this.group.scale.set(state.zoom, state.zoom, 1)
  }
  private handleHover(){
    // const vector = new Vector3(mouse.x, mouse.y, 1)
    // vector.unproject(this.camera)
    // const ray = new Raycaster(this.camera.position, vector.sub(this.camera.position).normalize())
    // const intersections = ray.intersectObjects(this.strips, true)
    // if(intersections.length > 0){
    //   const strip = intersections[0].object.parent as Strip
    //   strip.hovered = true
    // }
  }
}