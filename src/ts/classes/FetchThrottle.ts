export class FetchThrottle{
  private queue: Array<{input: RequestInfo, init?: RequestInit, resolve: (value: unknown) => void, reject: (reason?: any) => void }> = []
  private complete: Array<number> = []
  private inflight = 0
  constructor(
    private calls: number,
    private milliseconds: number
  ) {}
  private processQueue = () => {
    // Remove old complete entries.
    const now = Date.now();
    while (this.complete.length && this.complete[0] <= now - this.milliseconds)
      this.complete.shift();

    // Make calls from the queue that fit within the limit.
    while (this.queue.length && this.complete.length + this.inflight < this.calls) {
      let request = this.queue.shift();
      if(request){
        this.inflight++;
  
        // Call the deferred function, fulfilling the wrapper Promise
        // with whatever results and logging the completion time.
        let p = fetch(request.input, request.init)
        Promise.resolve(p).then((result) => {
          request?.resolve(result)
        }, (error) => {
          request?.reject(error)
        })
        .then(() => {
          this.inflight--;
          this.complete.push(Date.now());
          if (this.queue.length && this.complete.length === 1)
            setTimeout(this.processQueue, this.milliseconds);
        })
      }
      // Check the queue on the next expiration.
      if (this.queue.length && this.complete.length)
        setTimeout(this.processQueue, this.complete[0] + this.milliseconds - now);
    }
  }
  fetch(input: string, init?: RequestInit) {
    return new Promise((resolve, reject) => {
      this.queue.push({
        input,
        init,
        resolve,
        reject
      })
      this.processQueue()
    })
  }
}

