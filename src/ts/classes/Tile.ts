import { TimelineLite } from 'gsap'
import { Mesh, MeshBasicMaterial, PlaneGeometry, Texture, TextureLoader } from 'three'
import assets from '../services/assets'
import { scryFallService } from '../services/scryfall.service'
import { State } from './State'

const mask = new TextureLoader().load(assets.images['card_mask_small'])
const back = new TextureLoader().load(assets.images['card_back'])

export interface TileOptions {

}
export class Tile extends Mesh<PlaneGeometry, MeshBasicMaterial>{
  private _speed = 1
  private falloff = (
      State.tileCount * State.tileSize.w
      + State.tileCount * State.tileMargin ) /2
  constructor(w: number, h: number, options?: TileOptions){
    super()
    this.geometry = new PlaneGeometry(1, 1)
    this.material = new MeshBasicMaterial({
      color: 0xffffff,
      transparent: true,
      alphaMap: mask
    })
    this.scale.set(w, h, 1)
    this.material.opacity = 0
    this.recycle()
  }
  set speed(val: number) {
    this._speed = val
  }
  recycle(){
    this.getTexture()
    window.setTimeout(() => {this.recycle()}, Math.random()*60*1000 + 120*1000)
  }
  getTexture(){
    // for now just skip getting a new image until online again
    // recycle will call getTexture in another minute as normal
    if(!navigator.onLine || document.hidden) return;
    const arr = new Uint8Array(1);
    window.crypto.getRandomValues(arr);
    const returnBack = (arr[0]/255) <= 1/100
    if (returnBack){
      this.animate(back)
      return 
    }
    scryFallService.cardRandom()
    .then(card => {
      if (card){
        if (card.card_faces && card.card_faces.length > 0) {
          console.log("card has alternate faces")
          // card has alternate artworks, we need to pick one
          let idx = 0
          if (card.card_faces.length > 1) {
            const arr = new Uint8Array(1);
            window.crypto.getRandomValues(arr);
            idx = Math.round((arr[0]/255) * (card.card_faces.length-1))
          }
          card.image_uris = card.card_faces[idx].image_uris
        }
        if(card.image_uris && card.image_uris.normal){
          const url = card.image_uris.normal.replace(/(\?\d+)/, '') //stop cache busting images
          const tex = new TextureLoader().load(
            url,
            () => {
              this.animate(tex)
            }
          )
        } else {
          this.animate(back)
        }
      }
      else {
        this.animate(back)
      }
    })
  }
  animate(texture: Texture){
    const tl = new TimelineLite({ paused: true })

    if(this.material.opacity != 0)
      // only fade down if at 1
      tl.to(this.material, State.tileFade/1000, {opacity: 0})
    
    tl.add(() => {
      this.material.map = texture
      this.material.needsUpdate = true
    })
    .to(this.material, State.tileFade/1000, {opacity: 1})
    .play()
    document.addEventListener("visibilitychange", () => {
      if(document.hidden) {
        tl.pause()
      } else {
        tl.play()
      }
    })
  }
  update(){
    this.position.x -= 1 * this._speed
    
    if(this._speed < 0){
      if (this.position.x > this.falloff){
        this.position.x -= this.falloff * 2
      }
    } else {
      if (this.position.x < -this.falloff){
        this.position.x += this.falloff * 2
      }
    }
  }
}