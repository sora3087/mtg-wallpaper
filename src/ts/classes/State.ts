export class State {
  public static tileCount = 15
  public static tileSize = { w: 200, h: 200 * 7/5 }
  public static tileMargin = 20
  public static rowMargin = 20
  public static tileFade = 4000
}