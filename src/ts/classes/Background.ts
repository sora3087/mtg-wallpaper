import { TimelineLite } from 'gsap'
import { Color, Float32BufferAttribute, Material, Mesh, MeshBasicMaterial, PlaneGeometry } from 'three'

const black = new Color("black")

export class Background {
  private material: MeshBasicMaterial;
  private geometry: PlaneGeometry;
  public mesh: Mesh;
  private colors: Color[];
  private baseHue = Math.random() * 360
  private vertexColors: Float32BufferAttribute
  constructor(){
    this.material = new MeshBasicMaterial({
      vertexColors: true
    })
    this.colors = [
      new Color("black"),
      new Color("black"),
      new Color("black"),
      new Color("black"),
    ]
    for (let i = 0; i < this.colors.length; i++) {
      this.animateColor(i);
    }
    this.geometry = new PlaneGeometry()
    this.vertexColors = new Float32BufferAttribute([], 3)
    this.geometry.setAttribute("color", this.vertexColors)
    this.mesh = new Mesh(this.geometry, this.material)
    this.resize()
    this.update()
  }
  private animateColor(colorIdx: number){
    const target = new Color().setHSL(
      this.fuzzyAngle(),
      1,
      0.2
    )
    const a = { t: 0 }

    new TimelineLite({
      paused: true,
    })
    .to(
      a,
      20 + Math.random() * 10,
      {
        t: 1,
        onUpdate: () => {
          this.colors[colorIdx].lerpColors(black, target, a.t)
        }
      }
    )
    .to(
      a,
      20 + Math.random() * 10,
      {
        t: 0,
        onUpdate: () => {
          this.colors[colorIdx].lerpColors(black, target, a.t)
        }
      }
    )
    .add(() => {
      this.animateColor(colorIdx)
    })
    .play()
    
    this.baseHue += 5
    if(this.baseHue > 360)
      this.baseHue -= 360
  }
  private fuzzyAngle(){
    return this.baseHue + (Math.random() * 180 - 90)
  }
  private getColorFloats(){
    let ret: number[] = []
    this.colors.forEach(color => {
      ret = ret.concat(color.toArray())
    })
    return ret
  }
  public resize(){
    this.mesh.scale.set(window.innerWidth, window.innerHeight, 1)
  }
  public update(){
    this.geometry.setAttribute("color", new Float32BufferAttribute(this.getColorFloats(), 3))
  }
}