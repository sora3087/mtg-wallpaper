
export const mouse = {
  x: 0,
  y: 0,
  inView: false
}

function onDocumentMouseMove(event: MouseEvent) {
  // the following line would stop any other event handler from firing
  // (such as the mouse's TrackballControls)
  // event.preventDefault();

  // update the mouse variable
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  if(
    event.clientX >= 0
    && event.clientY >= 0
    && event.clientX < window.innerWidth
    && event.clientY < window.innerHeight
  ) {
    mouse.inView = true
  }
}

document.addEventListener('mousemove', onDocumentMouseMove, false);
document.addEventListener("mouseleave", function(event){

  if(
    event.clientY <= 0
    || event.clientX <= 0
    || ( event.clientX >= window.innerWidth
       || event.clientY >= window.innerHeight
    )
  ) {
    mouse.inView = false
  }
});