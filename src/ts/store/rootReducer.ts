import { combineReducers } from 'redux'
import { cardSettingsReducer } from './cards/reducers'
import { scryfallSettingsReducer } from './scryfall/reducers'
import { viewSettingsReducer } from './view/reducers'

export const rootReducer = combineReducers({
  card: cardSettingsReducer,
  view: viewSettingsReducer,
  scryfall: scryfallSettingsReducer
})