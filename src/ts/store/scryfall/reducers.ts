import { ScryfallActions, ScryfallState, SCRYFALL_SETTINGS_UPDATE } from './types'

const initialState: ScryfallState = {
  filter: ""
}

export function scryfallSettingsReducer(state = initialState, action: ScryfallActions){
  switch(action.type) {
    case SCRYFALL_SETTINGS_UPDATE: {
      const newState = {...state}
      let updated = false
      if (action.data.filter){
        newState.filter = action.data.filter.value
        updated = true
      }
      console.log(newState)
      if(updated) return newState;
      return state
    }
    default:
      return state
  }
}