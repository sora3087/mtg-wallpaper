import { TextProp } from '../../types/WPEPropTypes'

export const SCRYFALL_SETTINGS_UPDATE = "SCRYFALL_SETTINGS_UPDATE"

export interface ScryfallSettings {
  filter?: TextProp
}

type ScryfallSettingsAction = {
  type: typeof SCRYFALL_SETTINGS_UPDATE
  data: ScryfallSettings
}

export type ScryfallState = {
  filter: string
}

export type ScryfallActions = ScryfallSettingsAction