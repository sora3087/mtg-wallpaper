import { store } from '..'
import { ScryfallSettings, SCRYFALL_SETTINGS_UPDATE } from './types'

export function UpdateScryfallSettings(data: ScryfallSettings){
  store.dispatch({
    type: SCRYFALL_SETTINGS_UPDATE,
    data
  })
}