import { store } from '..'
import { ViewSettings, VIEW_SETTINGS_UPDATE } from './types'

export function UpdateViewSettings(data: ViewSettings){
  store.dispatch({
    type: VIEW_SETTINGS_UPDATE,
    data
  })
}