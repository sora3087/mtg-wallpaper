import { AxesHelper } from 'three'
import { ViewActions, ViewState, VIEW_SETTINGS_UPDATE } from './types'

const initialState: ViewState = {
  rotation: { x: 45, y: 0, z: 45 },
  rows: 2,
  cards: 15,
  zoom: 1
}

export function viewSettingsReducer(state = initialState, action: ViewActions){
  switch(action.type){
    case VIEW_SETTINGS_UPDATE: {
      const newState = {...state}
      let updated = false
      if(action.data.rotationX) {
        newState.rotation.x = action.data.rotationX.value
        updated = true
      } else if(action.data.rotationY) {
        newState.rotation.y = action.data.rotationY.value
        updated = true
      } else if(action.data.rotationZ) {
        newState.rotation.z = action.data.rotationZ.value
        updated = true
      }
      if(action.data.zoom){
        newState.zoom = action.data.zoom.value
        updated = true
      }
      if(action.data.rows){
        newState.rows = action.data.rows.value
        updated = true
      }
      if(updated){
        return newState
      } else {
        return state
      }
    }
    default:
      return state
  }
}