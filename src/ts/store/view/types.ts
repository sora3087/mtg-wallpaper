import { SliderProp } from '../../types/WPEPropTypes'

export const VIEW_SETTINGS_UPDATE = "VIEW_SETTINGS_UPDATE"

export interface ViewSettings {
  rotationX?: SliderProp
  rotationY?: SliderProp
  rotationZ?: SliderProp
  zoom?: SliderProp
  rows?: SliderProp
  cards?: number
}

type ViewSettingsAction = {
  type: typeof VIEW_SETTINGS_UPDATE
  data: ViewSettings
}

export type ViewState = {
  rotation: {
    x: number
    y: number
    z: number
  }
  rows: number
  cards: number
  zoom: number
}

export type ViewActions = ViewSettingsAction