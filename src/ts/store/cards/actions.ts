import { store } from '..'
import { CardSettings, CARD_SETTINGS_UPDATE } from './types'

export function UpdateCardSettings(data: CardSettings){
  store.dispatch({
    type: CARD_SETTINGS_UPDATE,
    data
  })
}