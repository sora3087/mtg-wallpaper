import { BoolProp, TextProp } from '../../types/WPEPropTypes'

export const CARD_SETTINGS_UPDATE = "CARD_SETTINGS_UPDATE"

export interface CardSettings {
  showBacks?: BoolProp
  width?: TextProp
  height?: TextProp
}

type CardSettingsAction = {
  type: typeof CARD_SETTINGS_UPDATE
  data: CardSettings
}

export type CardState = {
  width: number
  height: number
  showBacks: boolean
}

export type CardActions = CardSettingsAction