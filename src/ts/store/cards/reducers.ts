import { CardActions, CardState, CARD_SETTINGS_UPDATE } from './types'

const initialState: CardState = {
  width: 200,
  height: 200 * 7/5,
  showBacks: true
}

export function cardSettingsReducer(state = initialState, action: CardActions){
  switch(action.type){
    case CARD_SETTINGS_UPDATE: {
      const newState = {...state}
      let updated = false
      if(action.data.width){
        newState.width = parseInt(action.data.width.value)
        updated = true
      }
      if(action.data.height){
        newState.height = parseInt(action.data.height.value)
        updated = true
      }
      if(action.data.showBacks){
        newState.showBacks = action.data.showBacks.value
        updated = true
      }
      if(updated) return newState;
      return state
    }
    default:
      return state
  }
}