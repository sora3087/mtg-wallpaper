type BaseProp = {
  index: number
  order: number
  key: string
  type: string
  text: string
}

type ComboOption = {
  label: string
  value: string
}

export type ComboProp = {
  icon: string
  type: "combo"
  options: Array<ComboOption>
  value: string
}

export type ColorProp = BaseProp & {
  type: "color"
  index: undefined
  value: string
}

export type SliderProp = BaseProp & {
  type: "slider"
  fraction: boolean
  max: number
  min: number
  precision: number
  step: number
  value: number
}

export type TextProp = BaseProp & {
  type: "textinput"
  value: string
}

export type BoolProp = BaseProp & {
  type: "bool"
  value: boolean
}