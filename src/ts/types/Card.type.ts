export type Legality = 'legal' | 'not_legal'

export type CardFace = {
  artist: string
  artist_id: string
  colors: []
  illustration_id: string
  image_uris: CardImages
  mana_cost: string
  name: string
  object: "card_face"
  oracle_text: string
  type_line: string
}

export type CardImages = {
  small: string
  normal: string
  large: string
  png: string
  art_crop: string
  border_crop: string
}

export type Card = {
  object: "card"
  id: string
  oracle_id: string
  multiverse_ids: number[]
  mtgo_id: number
  mtgo_foil_id: number
  tcgplayer_id: number
  cardmarket_id: number
  name: string
  lang: string
  released_at: string
  uri: string
  scryfall_uri: string
  layout: string
  highres_image: boolean,
  card_faces?: Array<CardFace>
  image_uris?: CardImages,
  mana_cost: string,
  cmc: number,
  type_line: string,
  oracle_text: string
  power: string
  toughness: string
  colors: string[]
  color_identity: string[]
  keywords: []
  legalities: {
    standard: Legality
    future: Legality
    historic: Legality
    gladiator: Legality
    pioneer: Legality
    modern: Legality
    legacy: Legality
    pauper: Legality
    vintage: Legality
    penny: Legality
    commander: Legality
    brawl: Legality
    duel: Legality
    oldschool: Legality
    premodern: Legality
  },
  games: string[]
  reserved: boolean
  foil: boolean
  nonfoil: boolean
  oversized: boolean
  promo: boolean
  reprint: boolean
  variation: boolean
  set: string
  set_name: string
  set_type: string
  set_uri: string
  set_search_uri: string
  scryfall_set_uri: string
  rulings_uri: string
  prints_search_uri: string
  collector_number: string
  digital: boolean
  rarity: string
  flavor_text: string
  card_back_id: string
  artist: string
  artist_ids: string[]
  illustration_id: string
  border_color: string
  frame: string
  full_art: boolean
  textless: boolean
  booster: boolean
  story_spotlight: boolean
  edhrec_rank: number
  prices: {
    usd: string
    usd_foil: string
    eur: string
    eur_foil: string
    tix: string
  },
  related_uris: {
    gatherer: string
    tcgplayer_decks: string
    edhrec: string
    mtgtop8: string
  },
  purchase_uris: {
    tcgplayer: string
    cardmarket: string
    cardhoarder: string
  }
}